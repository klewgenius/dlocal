# dlocal Assessment #

### Requirements ###

- Java SE Development Kit 8 Downloads
- Java SE Runtime Environment 8

### How to run it? ###

1) Clone the repository from Bitbucket

```
git clone https://klewgenius@bitbucket.org/klewgenius/dlocal.git

```
2) Configure variables file

Open ``` Dlocal.Assessment\dist\config.properties ``` and specify your own values.
The keyword you want to find; the location of your input and the folder where you want to place your output

![Alt text](https://bytebucket.org/klewgenius/dlocal/raw/e7c2fe82e4cbaae453b639717409324ae9eda75e/Readme%20images/img1.PNG)

IMPORTANT!

For Windows users; if you want to use backslash you will have to escape it with another \

Example:
```
C:\\Users\\abaez\\Documents\\NetBeansProjects\\Dlocal.Assessment\\dist\\Output\\
```


3) Run the application

- Go to the distribution folder ```Dlocal.Assessment\dist```
- Run it!

```
java -jar Dlocal.Assessment.jar <location of config.properties>

```

Example:

```
java -jar Dlocal.Assessment.jar C:\Users\abaez\Documents\NetBeansProjects\Dlocal.Assessment\dist\config.properties

```

4) Open your output file.

![Alt text](https://bytebucket.org/klewgenius/dlocal/raw/720b6083f49883074733373b251edb568429a2cb/Readme%20images/img2.PNG)

As you can see above the results are printed in the console but also an output file is created in the location you specified.


### Algorithm and Performance ###


I added some Unit Test to measure the performance for different cases (including cases of 400 and 500 characters) and the results are:


![Alt text](https://bytebucket.org/klewgenius/dlocal/raw/720b6083f49883074733373b251edb568429a2cb/Readme%20images/img3.PNG)

The problem was resolved using Tabulation (technique used for caching previous computed results) to avoid the overhead of recursion.

Time complexity: O(MN) when N is the length of the keyword ("welcome to dlocal") and M the entry we are analyzing.


### Code style ###

The code was written using SOLID principles that allow us to create robust and reusable components.
It´s easy to maintain and ready to be tested.


