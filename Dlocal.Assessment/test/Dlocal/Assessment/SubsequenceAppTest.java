/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package Dlocal.Assessment;
import dataaccess.FileManager;
import dataaccess.IFileManager;
import domain.Calculator;
import domain.ICalculator;
import main.SubsequenceApp;
import static org.mockito.Mockito.*;

import org.junit.Test;

/**
 *
 * @author abaez
 */
public class SubsequenceAppTest {
    
    IFileManager fileManager;
    ICalculator calculator;
    
    public SubsequenceAppTest() {
        fileManager = mock(FileManager.class);
        calculator = spy(new Calculator());
        when(fileManager.Keyword()).thenReturn("welcome to dlocal");
        
    }
    
    @Test
    public void SubsequenceApp_WhenFiveInputs_EvaluateIndividualEntryCalledFiveTimes() {
        String[] input = new String[]{"5", "welcome to dlocal", "dlocal","o dlocal","welcocal","welcal"};
        
        when(fileManager.ReadInputFile()).thenReturn(input);
        
        SubsequenceApp app = new SubsequenceApp(calculator, fileManager);
        app.Run();
        
        verify(calculator, times(5)).EvaluateIndividualEntry(any(), any());
    }
    
    @Test
    public void SubsequenceApp_WhenInvalidInputs_EvaluateIndividualEntryNeverCalled() {
        String[] input = new String[]{"3", "welcome to dlocal", "dlocal","o dlocal","welcocal","welcal"};
        
        when(fileManager.ReadInputFile()).thenReturn(input);
        
        SubsequenceApp app = new SubsequenceApp(calculator,fileManager);
        app.Run();
                
        verify(calculator, never()).EvaluateIndividualEntry(any(),any());
    }
    
    @Test
    public void SubsequenceApp_WhenEmptyInputs_EvaluateIndividualEntryNeverCalled() {
        String[] input = new String[]{};
        
        when(fileManager.ReadInputFile()).thenReturn(input);
        
        SubsequenceApp app = new SubsequenceApp(calculator,fileManager);
        app.Run();
        
        verify(calculator, never()).EvaluateIndividualEntry(any(),any());
    }
}
