/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dlocal.Assessment;

import domain.Calculator;
import domain.ICalculator;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author abaez
 */
public class EvaluateIndividualEntryTest {
    
    ICalculator calculator;
    String keyword1 = "welcome to dlocal";
    
    public EvaluateIndividualEntryTest() {
        calculator = new Calculator();
    }
        
    @Test
    public void EvaluateIndividualEntry_WhenTextsAreEquals_ReturnsOne() {
        int result = calculator.EvaluateIndividualEntry(keyword1, "welcome to dlocal");
        assertEquals(1, result);
    }
        
    @Test
    public void EvaluateIndividualEntry_WhenNoSpaces_ReturnsZero() {
        int result = calculator.EvaluateIndividualEntry(keyword1, "welcome todlocal");
        assertEquals(0, result);
    }
    
    @Test
    public void EvaluateIndividualEntry_When768Example_Returns768() {
        int result = calculator.EvaluateIndividualEntry(keyword1, "wweellcccoommee​ ​to​ ​code​ ​d​ ​other​ ​local");
        assertEquals(768, result);
    }
    
    @Test
    public void EvaluateIndividualEntry_WhenEmptyEntry_ReturnsZero() {
        int result = calculator.EvaluateIndividualEntry(keyword1, "");
        assertEquals(0, result);
    }
    
    @Test
    public void EvaluateIndividualEntry_With70Characters() {
        int result = calculator.EvaluateIndividualEntry(keyword1, "wwwwweeeeeeeellllllllcccccccccccoommee​ ​to​ ​code​ ​d​ ​other​ ​local");
        assertEquals(112640, result);
    
    }
    
    @Test
    public void EvaluateIndividualEntry_With400Characters() {
        int result = calculator.EvaluateIndividualEntry(keyword1, "wwwwweeeeeeeellllllllllllllllllllllllllllllllllcccccccccccoommee​ ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt​to​ ​coooooooooooooooooooooooooooode​ ​d​dddddddddddddddddddddddddddddddddddddd ​otttttttttthhhhhhhhhhhhhhhhhhhhhhhhhhhheeeeeeeeeeeeeeeeeeeeer​ ​locaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaallllllllllllllllllllllllllllllllllllllllllllllllllllllll");
        assertEquals(2035687424, result);
    }
    
    //@Test
    public void EvaluateIndividualEntry_With500Characters() {
        int result = calculator.EvaluateIndividualEntry(keyword1, "wwwwweeeeeeeellllllllllllllllllllllllllllllllllcccccccccccoommmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmeeeeeeeeeeeeeeee​ ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt​to​ ​coooooooooooooooooooooooooooode​ ​d​dddddddddddddddddddddddddddddddddddddd ​otttttttttthhhhhhhhhhhhhhhhhhhhhhhhhhhheeeeeeeeeeeeeeeeeeeeer​ ​looooooooooooooooooooooooooooooooooocaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaallllllllllllllllllllllllllllllllll");
        assertEquals(2035687424, result);
    }
}
