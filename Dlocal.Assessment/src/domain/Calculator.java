/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package domain;

/**
 *
 * @author abaez
 */
public class Calculator implements ICalculator {
    
        
    /*
    Example of input:
    
    3
    lala​ ​w​ ​el​ ​come​ ​to​ ​dlocal
    wweellcccoommee​ ​to​ ​code​ ​d​ ​other​ ​local
    welcome​ ​todlocal
    
    The first value in the Array indicates the amount of entries.
    The rest is the list of records we need to evaluate.
    */
    @Override
    public int[] EvaluateAllEntries(String keyword, String[] entries)
    {
        if(ValidateInput(entries))
        {
            int[] result = new int[entries.length - 1];

            //Let´s skip the first one, which we don´t need to evaluate.
            for (int i = 1; i < entries.length; i++) {
                String entry = entries[i];
                if(entry.length() <= 500)
                    result[i -1] = EvaluateIndividualEntry(keyword, entry);
                else
                    result[i -1] = -1; // we won´t analyze strings longer than 500 characters
            }
            return result;
        }
        else
        {
            return new int[0]; 
            /* That was made for simplification. It´s not a good practice.
               This needs to be changed for an appropiate exception. 
               The parent method doesn´t know if a "int[0]" is a valid result or an error.
             */
        }
        
    }
    
    @Override
    public int EvaluateIndividualEntry(String keyword, String entry)
    {        
        String[] a = entry.split("");
        String[] b = keyword.split("");
        int subsequences = CalculateSubsecuences(a, b, a.length, b.length);
        return subsequences;
    }
    
    
    public int CalculateSubsecuences(String[] a, String[] b,int m, int n )
    {
        
        // Create a table to store results of sub-problems
        int[][] lookup = new int[m + 1][n + 1];
        
        // If first string is empty
        for (int i = 0; i <= n; ++i)
            lookup[0][i] = 0;
        
        // If second string is empty
        for (int i = 0; i <= m; ++i)
            lookup[i][0] = 1;
        
        // Fill lookup[][] in bottom up manner
        for (int i = 1; i <= m; i++)
        {
            for (int j = 1; j <= n; j++)
            {
                // If last characters are same, we have two
                // options -
                // 1. consider last characters of both strings
                //    in solution
                // 2. ignore last character of first string
                if (a[i - 1].equals(b[j - 1]))
                    lookup[i][j] = lookup[i - 1][j - 1] +
                            lookup[i - 1][j];
                
                else
                    // If last character are different, ignore
                    // last character of first string
                    lookup[i][j] = lookup[i - 1][j];
            }
        }
        
        return lookup[m][n];
    }
    
    
    private Boolean ValidateInput(String[] entries)
    {
        if(entries == null 
                || entries.length <= 2){
            System.out.println("Provide at least two lines");
            return false;
        }
        
        if(entries.length > 101)
        {
            System.out.println("No more than 100 lines are allowed.");
            return false;
        }
        
        try{
            int amount = Integer.parseInt(entries[0]);
            if(amount != entries.length - 1){
                System.out.println("Number of lines specified is different than the amount of lines in the file.");
                return false;
            }
        }
        catch(Exception ex){
            System.out.println("Input format not valid");
            return false;
        }
        
        return true;
    }
}
