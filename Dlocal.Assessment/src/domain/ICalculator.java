/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

/**
 *
 * @author abaez
 */
public interface ICalculator {
    
    int[] EvaluateAllEntries(String keyword, String[] entries);
    int EvaluateIndividualEntry(String keyword, String entry);
    
}
