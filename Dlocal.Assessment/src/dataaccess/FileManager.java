/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package dataaccess;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.Path;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 *
 * @author abaez
 */
public class FileManager implements IFileManager {
        
    private Map<String, String> configurationProperties = null;
    private static FileManager instance;
    
    private FileManager(String configFile){ 
        
        // We don´t want to read the configuration file every time we need a value.
        // We will read it only once in the constructor.
        
        // From Unit Tests, I don´t want to read a configuration file, so I can pass an empty string.
        if(!configFile.isEmpty())
            configurationProperties = ReadConfigFile(configFile);
        
    }
    
    public static synchronized FileManager getInstance(String configFile){
        if(instance == null){
            instance = new FileManager(configFile);
        }
        return instance;
    }
    
    @Override
    public String Keyword()
    {
        return configurationProperties.get("keyword");
    }
    
    private String OutputFolder()
    {
        return configurationProperties.get("outputFolderLocation");
    }
    
    private String InputFileLocation()
    {
        return configurationProperties.get("inputFileLocation");
    }
        
    private Map<String, String> ReadConfigFile(String configFile)
    {
        Map<String, String> response = new HashMap<>();
        InputStream fileInput;
        try
        {
            File file = new File(configFile);
            fileInput = new FileInputStream(file);
 
            Properties properties = new Properties();
            properties.load(fileInput);
            fileInput.close();
            
            Enumeration enuKeys = properties.keys();
            while (enuKeys.hasMoreElements()) {
                String key = (String) enuKeys.nextElement();
                String value = properties.getProperty(key);
                response.put(key, value);
            }
        }
        catch (Exception ex){
            System.out.println("Error while ReadConfigFile: " + ex.getMessage());
        }
        
        return response;
    }
    
    
    @Override
    public String[] ReadInputFile()
    {
        List<String> response = new ArrayList();
        
        try
        {
            String inputLocation = InputFileLocation();
            
            // FileReader reads text files in the default encoding.
            FileReader fileReader = new FileReader(inputLocation);
            
            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            
            while((line = bufferedReader.readLine()) != null) {
                response.add(line);
            }
            // Always close files.
            bufferedReader.close();
        }
        catch (Exception ex)
        {
            System.out.println("Error while ReadInputFile: " + ex.getMessage());
        }
        
        return response.stream().toArray(String[]::new);
    }
    
    /*
    Given a list of results, this method gives the format "Case #1: 0001" to be printed.
    */
    private List<String> ParseResult(int[] result)
    {
        List<String> lines = new ArrayList();
        for (int i = 0; i < result.length; i++) {
            DecimalFormat df = new DecimalFormat("0000");
            int r = result[i];
            
            if(r == -1)
            {
                // (-1) This input was not read.

                String formatedText = "Case #" + (i + 1) + " -- Skipped (more than 500 characters)";
                System.out.println(formatedText);
                lines.add(formatedText);
            }
            else
            {
                String formatedNumber = df.format(r);
                String lastFourDigits = formatedNumber.substring(Math.max(0, formatedNumber.length() - 4));

                String formatedText = "Case #" + (i + 1) + ": " + lastFourDigits;

                System.out.println(formatedText);

                lines.add(formatedText);
            }
        }
        return lines;
    }
    
    @Override
    public Boolean WriteOutput(int[] result)
    {
        
        System.out.println("Results:");
        
        String outputFolder = OutputFolder();
        List<String> lines = ParseResult(result);
        String fileName = "result_" + System.currentTimeMillis();
        String path = outputFolder + fileName + ".out";
        
        try
        {
            
            try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(path), "utf-8"))) {
                
                lines.forEach(ln -> {
                    try {
                        writer.write(ln + "\n");
                    } catch (IOException ex) {
                        // Let´s ignore to simplify
                    }
                });                
            }            
        }
        catch (Exception ex)
        {
            System.out.println("Error: " + ex.getMessage());
            // Let´s ignore to simplify
            return false;
        }
        
        System.out.println("Output file: " + path);
        
        return true;        
    }
    
}
