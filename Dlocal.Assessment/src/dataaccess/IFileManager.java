/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataaccess;

import java.io.IOException;
import java.util.Map;

/**
 *
 * @author abaez
 */
public interface IFileManager {

    String[] ReadInputFile();
    String Keyword();
    Boolean WriteOutput(int[] result);
}
