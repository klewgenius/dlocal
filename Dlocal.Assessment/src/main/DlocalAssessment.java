package main;

/*
* To change this license header, choose License Headers in Project Properties.

* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
import domain.Calculator;
import domain.ICalculator;
import dataaccess.FileManager;
import dataaccess.IFileManager;
import java.util.List;

/**
 *
 * @author abaez
 */
public class DlocalAssessment {
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        // The interfaces resolutions are usually made automatically by a Dependency Injection Container.
        // To simplify; I´m resolving them manually here.
        ICalculator calculatorSvc = new Calculator();
        IFileManager fileManager = FileManager.getInstance(args[0]);
                
        SubsequenceApp app = new SubsequenceApp(calculatorSvc, fileManager);
        app.Run();
        
    }
    
}
