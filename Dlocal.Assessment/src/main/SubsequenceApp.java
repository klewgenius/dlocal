/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import domain.ICalculator;
import dataaccess.IFileManager;

/**
 *
 * @author abaez
 */
public class SubsequenceApp {
    
    public ICalculator _calculatorService;
    public IFileManager _fileReaderService;
    
    public SubsequenceApp(ICalculator calculator, IFileManager fileReader){
        this._calculatorService = calculator;
        this._fileReaderService = fileReader;
    }
    
    public void Run()
    {
        Boolean success = true;
        System.out.println("Application started.");
        
        // 1) Read configuration file and all entries
        String[] input = _fileReaderService.ReadInputFile();
        
        // 2) Validate entries and Evaluate
        int[] result = _calculatorService.EvaluateAllEntries(_fileReaderService.Keyword(), input);
        
        // 3) Save to file
        if(result.length > 0)
            success = _fileReaderService.WriteOutput(result);
        
        System.out.println("Application closed. Success: " + success);
    }
}